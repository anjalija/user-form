from django.core.checks import messages
from django.shortcuts import redirect, render
from django.views.generic import View
from .models import *
from django.contrib import messages
import datetime
from django.core.mail import send_mail
from django.conf import settings




# Create your views here.
#redirect a user-form
class Home(View):
    def get(self,request):
        return redirect('/user-form/')

#user-form save 
class UserForm(View):
    def get(self,request):
        return render(request,'user-form.html')
    def post(self,request):
        birthdate = datetime.datetime.strptime(request.POST.get('dob'),'%Y-%m-%d')
        #validate a data
        number_validate = ['6','7','8','9']
        print(list(request.POST.get('contact_no'))[0],len(request.POST.get('contact_no')))
        if len(request.POST.get('contact_no')) > 10:
            messages.error(request,'Please Check Contact Number')
            return render(request,'user-form.html',{'data':request.POST})
        if list(request.POST.get('contact_no'))[0] not in number_validate:
            messages.error(request,'Please Check Contact Number. Enter correct Contact Number')
            return render(request,'user-form.html',{'data':request.POST})
        if UserModel.objects.filter(email=request.POST.get('email')):
            messages.error(request,'Email ID Already exist')
            return render(request,'user-form.html',{'data':request.POST})
        if UserModel.objects.filter(contact_number=request.POST.get('contact_no')):
            messages.error(request,'Contact Number Already exist')
            return render(request,'user-form.html',{'data':request.POST})
        if (datetime.datetime.today() - birthdate) < datetime.timedelta(days=18*365):
            messages.error(request,'Must be at least 18 years old to register')
            return render(request,'user-form.html',{'data':request.POST})
        #save the data 
        user = UserModel()
        user.name = request.POST.get('name')
        user.email = request.POST.get('email')
        user.contact_number = request.POST.get('contact_no')
        user.date_of_birth = request.POST.get('dob')
        user.save()
        #send a email 
        subject = 'Regisration Mail!'
        message = f'hey {user.name}, You Have successfully Registered. Thanks for connecting us.'
        email_from = settings.EMAIL_HOST_USER
        recipient_list = [request.POST.get('email'), ]
        send_mail(subject, message, email_from, recipient_list)

        return redirect('/user-info/')

#get user-info
class UserInfo(View):
    def get(self,request):
        user= UserModel.objects.all()
        return render(request,'user-info.html',{'user':user})