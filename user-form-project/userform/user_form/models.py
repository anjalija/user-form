from django.db import models

# Create your models here.
class UserModel(models.Model):
    name = models.CharField(max_length=25)
    email = models.EmailField(max_length=255, unique=True)
    contact_number = models.CharField(max_length=12)
    date_of_birth = models.DateField(null=True, blank=True)

    def __str__(self):
        return self.name
