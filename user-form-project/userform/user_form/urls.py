from django.urls import path
from .views import *
urlpatterns = [
    path('user-form/', UserForm.as_view(), name="user-form"),
    path('', Home.as_view(), name="home"),
    path('user-info/', UserInfo.as_view(), name="user-info"),

]